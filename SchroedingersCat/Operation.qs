﻿namespace Quantum.SchroedingersCat
{
    open Microsoft.Quantum.Primitive;
    open Microsoft.Quantum.Canon;

	operation Set (qubit: Qubit, expected: Result) : ()
	{
		body
		{
			if(M(qubit) != expected)
			{
				X(qubit);
			}
		}
	}

	operation Experiment () : (Int, Int, Int, Int)
    {
        body
        {
			mutable alive = 0;
			mutable dead = 0;
			mutable decayed = 0;
			mutable agreed = 0;
			using(registers = Qubit[2])
			{
				for(counter in 1.. 100)
				{
					let cat = registers[0];
					let atom = registers[1];

					H(atom);
					CNOT(atom, cat);

					if(M(cat) == Zero) { set alive = alive + 1; }
					if(M(cat) == One) { set dead = dead + 1; }
					if(M(atom) == One) {set decayed = decayed +1; }
					if(M(atom) == M(cat)) {set agreed = agreed + 1;}
					Set(cat, Zero);
					Set(atom, Zero);
				}
			}
			return (alive,dead,decayed,agreed);
        }
    }
}
