﻿using Microsoft.Quantum.Simulation.Core;
using Microsoft.Quantum.Simulation.Simulators;
using System;

namespace Quantum.SchroedingersCat
{
    class Driver
    {
        static void Main(string[] args)
        {
            using (var box = new QuantumSimulator())
            {
                var result = Experiment.Run(box);

                long aliveCats, deadCats, decayedAtoms, inAgreement;
                (aliveCats, deadCats, decayedAtoms, inAgreement) = result.Result;

                Console.WriteLine(
                    string.Format("Alive cats: {0}\t| Dead cats: {1}\t| Decayed atoms: {2}\t| In agreement: {3}",
                    aliveCats, deadCats, decayedAtoms, inAgreement));
            }

            Console.WriteLine("Press any key...");
            Console.ReadLine();
        }
    }
}